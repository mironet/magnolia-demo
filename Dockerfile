# Get magnolia webapp
FROM alpine:3.10 AS builder
WORKDIR /app

ARG MAGNOLIA_VERSION=6.2.45

# RUN mvn -C -B dependency:get \
#     -DremoteRepositories=https://nexus.magnolia-cms.com/content/repositories \
#     -Ddest=./ \
#     -Dartifact=info.magnolia.bundle:magnolia-community-demo-webapp:${MAGNOLIA_VERSION}:jar \
#     -Dtransitive=false \
#     -Ddest=magnolia.jar

RUN apk add curl unzip && \
    curl -v https://nexus.magnolia-cms.com/repository/public/info/magnolia/bundle/magnolia-community-demo-webapp/${MAGNOLIA_VERSION}/magnolia-community-demo-webapp-${MAGNOLIA_VERSION}.war --output magnolia.war

RUN ls -Fahl . && \
    mkdir magnolia/ && \
    unzip magnolia.war -d magnolia/ && \
    rm -vf magnolia.war

FROM busybox:latest AS final

COPY --from=builder /app/ /
